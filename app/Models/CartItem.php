<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'user_id',
        'cart_id',
        'product_id',
        'quantity',
        'total',
    ];

    /** 
     * 
     * Relationships
     * 
     */
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
