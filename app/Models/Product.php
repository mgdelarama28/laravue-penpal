<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'stock',
    ];

    /** 
     * 
     * Relationships
     * 
     */
    public function cartItem()
    {
        return $this->hasOne(CartItem::class);
    }
}
